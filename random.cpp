#include "precomp.h"

/*
* https://www.johndcook.com/blog/cpp_TR1_random/
*/

float Random::RandomFloat(float min, float max)
{
	std::tr1::uniform_real<float> dist(min, max);

	return dist(rng);
}

int Random::RandomInt(int min, int max)
{
	std::tr1::uniform_int<int> dist(min, max);

	return dist(rng);
}

/*
* https://math.stackexchange.com/questions/1163260/random-directions-on-hemisphere-oriented-by-an-arbitrary-vector
*/
vec3 Random::RandomPointOnHemisphere(vec3 direction)
{
	vec3 p;

	float d;
	vec3 other;
	
	if (direction.z == 0 && direction.y == 0)
		other = vec3(direction.y, direction.x, direction.z);	
	else
		other = vec3(direction.x, direction.z, direction.y);

	vec3 right = cross(direction, other).normalized();
	vec3 up = cross(direction, right).normalized();

	do {
		p = RandomFloat(FLT_MIN, 1.0f - FLT_MIN) * direction;
		p += RandomFloat(-1.0f + FLT_MIN, 1.0f - FLT_MIN) * right;
		p += RandomFloat(-1.0f + FLT_MIN, 1.0f - FLT_MIN) * up;
		d = p.length();
	} while (d > 1);

	// Normalize it
	p.x = p.x / d;
	p.y = p.y / d;
	p.z = p.z / d;

	return p;
}
#pragma once

class Random {
public:
	float RandomFloat(float min, float max);
	int RandomInt(int min, int max);
	vec3 RandomPointOnHemisphere(vec3 direction);
private:
	std::tr1::mt19937 rng;
};
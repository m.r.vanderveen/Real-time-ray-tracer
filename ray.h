#pragma once

class Ray
{
public:
	void Cast(Camera* cam, float u, float v);
	void Reset(vec3 d);
	void Reflect(vec3 normal);
	void DiffuseReflect(vec3 normal);
	void CosineWeightedDiffuseReflection(vec3 normal);
	vec3 TransformToTangent(vec3 normal, vec3 worldVector);
	float Refract(vec3 &normal, float n1divn2, float n2divn1);
	bool isInsideObject = false;
	vec3* origin;
	float t;
	vec3 dir;
private:
	Random rng;
};
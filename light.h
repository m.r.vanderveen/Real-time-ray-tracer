#pragma once

class Light
{
public:
	Light(vec3 position, vec3 col, float i) { pos = position; color = col; intensity = i; };
	bool Intersect();
	vec3 pos, color;
	float intensity;
};
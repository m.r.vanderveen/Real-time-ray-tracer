#include "precomp.h" 

RayTracer::RayTracer(Camera* cam, std::vector<Geometry*> &scn, std::vector<Triangle*> &tris, BVH* b)
{
	camera = cam;
	scene = scn;
	triangles = tris;
	bvh = b;

	lights.push_back(new Light(vec3(-550.0f, 1050.0f, 50.0f), vec3(1.0f, 1.0f, 1.0f), 500000.0f));
	lights.push_back(new Light(vec3(650.0f, 1050.0f, 50.0f), vec3(1.0f, 1.0f, 1.0f), 500000.0f));

	// Pre-calculate u,v coordinates
	ucoords = new float[SCRWIDTH];
	vcoords = new float[SCRHEIGHT];
	float u = 0;
	float v = 0;
	float uinc = 1.0f / SCRWIDTH;
	float vinc = 1.0f / SCRHEIGHT;

	for (int i = 0; i < SCRWIDTH; i++) {
		ucoords[i] = u;
		u += uinc;
	}
	for (int i = 0; i < SCRHEIGHT; i++) {
		vcoords[i] = v;
		v += vinc;
	}

	ray = new Ray();
	shadowRay = new Ray();
	reflectRay = new Ray();
}

void RayTracer::Render(Surface* screen)
{
	// Trace rays and plot pixels to graphics window
	if (partition) {
		rayIndex = new uint[SCRHEIGHT * SCRWIDTH];
		for (int i = 0; i < SCRHEIGHT * SCRWIDTH; i++)
			rayIndex[i] = i;
	}

	// Loop through vertical pixels
	for (int i = 0; i < SCRHEIGHT; i++) {
		// Loop through horizontal pixels
		for (int j = 0; j < SCRWIDTH; j++)
		{
			if (!partition) {
				ray->origin = &(camera->pos);
				ray->Cast(camera, ucoords[j], vcoords[i]);

				// Trace ray
				recursionCntr = 0;
				vec3 color = Trace(ray);

				// Plot ray color
				screen->Plot(SCRWIDTH - j, SCRHEIGHT - i, ((int)(min(1.0f, color.x) * 255.0f) << 16) + ((int)(min(1.0f, color.y) * 255.0f) << 8) + (int)(min(1.0f, color.z) * 255.0f));

			}
			else {
				rayCollection[j + i * SCRWIDTH] = new Ray();
				rayCollection[j + i * SCRWIDTH]->origin = &(camera->pos);
				rayCollection[j + i * SCRWIDTH]->Cast(camera, ucoords[j], vcoords[i]);
			}
		}
	}

	if (partition) {
		uint activePointer = PartitionRays(rayCollection, rayIndex, bvh);

		for (uint i = 0; i < activePointer; i++)
		{
			Ray* currentRay = rayCollection[rayIndex[i]];
			// Trace ray
			recursionCntr = 0;
			vec3 color = Trace(currentRay);

			// Plot ray color
			screen->Plot(SCRWIDTH - (rayIndex[i] % SCRWIDTH), SCRHEIGHT - (rayIndex[i] / SCRWIDTH), ((int)(min(1.0f, color.x) * 255.0f) << 16) + ((int)(min(1.0f, color.y) * 255.0f) << 8) + (int)(min(1.0f, color.z) * 255.0f));
		}
	}
}

uint RayTracer::PartitionRays(Ray** rayCollection, uint* &rayIndex, BVH* bvh)
{
	uint activePointer = 0;
	for (int i = 0; i < SCRWIDTH * SCRHEIGHT; i++)
	{
		if (bvh->root->Intersect(rayCollection[rayIndex[i]]))
		{
			uint temp = rayIndex[i];
			rayIndex[i] = rayIndex[activePointer];
			rayIndex[activePointer] = temp;
			activePointer++;
		}
	}
	return activePointer;
}

vec3 RayTracer::Trace(Ray* r)
{
	if (recursionCntr >= RECURSION_LIMIT)
		return vec3(1, 1, 1);
	recursionCntr++;

	vec3 intersectPoint = NULL;
	Material* material;
	vec3 normal;

	// Loop through non-triangle objects in scene
	for (uint k = 0; k < scene.size(); k++)
	{
		scene[k]->Intersect(r, intersectPoint, material, normal);
	}

	// Loop through triangles using a BVH
	bvh->Traverse(r, triangles, intersectPoint, material, normal);

	if (intersectPoint.x == NULL)
		return vec3(0, 0, 0);

	switch (material->surface)
	{
	case Material::DIFFUSE:
		return material->color * DirectIllumination(intersectPoint, normal);
	case Material::MIRROR: {
		r->Reflect(normal);
		vec3 reflectPoint = intersectPoint + (r->dir * EPSILON);
		r->origin = &reflectPoint;

		return material->color * Trace(r);
	}
	case Material::GLOSSY: {
		float s = material->specularity;
		float d = 1 - s;
		r->Reflect(normal);
		vec3 reflectPoint = intersectPoint + (r->dir * EPSILON);
		r->origin = &reflectPoint;

		return material->color * (s * Trace(r) + d * DirectIllumination(intersectPoint, normal));
	}
	case Material::GLASS: {
		memcpy(reflectRay, r, sizeof(Ray));
		reflectRay->Reflect(normal);
		vec3 reflectPoint = intersectPoint + (r->dir * EPSILON);
		r->origin = &reflectPoint;

		float R0;
		if (r->isInsideObject)
			R0 = material->R1;
		else
			R0 = material->R0;

		float cosI = r->Refract(normal, material->n1divn2, material->n2divn1);
		if (cosI == -1)
			return vec3(0, 0, 0);

		vec3 refractPoint = intersectPoint + (r->dir * EPSILON);
		r->origin = &refractPoint;

		float Fr = 0.2f;// R0 + (1.0f - R0) * pow(1.0f - cosI, 5);

		return Fr * Trace(reflectRay) + (1.0f - Fr) * Trace(r);
	}
	default:
		return vec3(0, 0, 0);
	}
}

vec3 RayTracer::DirectIllumination(vec3 pos, vec3 norm)
{
	vec3 color = vec3(0.0f);

	for (uint i = 0; i < lights.size(); i++)
	{
		vec3 L = lights[i]->pos - pos;

		float distToLight = L.length();

		vec3 newOrigin = pos + (L * EPSILON_SHAD);
		shadowRay->origin = &newOrigin;
		L.normalize();

		shadowRay->t = FLT_MAX;
		shadowRay->dir = L;
		float t;
		bool intersects = false;

		Material* mat;
		vec3 poi;
		vec3 normal;
		bvh->Traverse(shadowRay, triangles, poi, mat, normal);
		if (shadowRay->t >= 0 && shadowRay->t != FLT_MAX && shadowRay->t < distToLight)
			intersects = true;

		for (uint k = 0; k < scene.size(); k++)
		{
			t = scene[k]->Intersect(shadowRay);
			if (t >= 0 && t < distToLight) {
				intersects = true;
				break;
			}
		}

		if (intersects) continue;

		float angle = dot(L, norm);
		if (angle < 0.0f)
			angle = dot(L, -norm);

		color += lights[i]->color * ((lights[i]->intensity * angle) / (distToLight * distToLight));
	}

	return color;
}
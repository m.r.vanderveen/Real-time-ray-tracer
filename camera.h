#pragma once

class Camera
{
public:
	Camera(vec3 position, vec3 direction, float distance);
	void Update();
	bool Rotate(float speed);
	bool ChangeFOV();
	bool Translate(float speed);
	vec3 center;
	vec3 p0, p1, p2;
	vec3 pos;
	vec3 dir;
	vec3 right, up;
	float dist;
};
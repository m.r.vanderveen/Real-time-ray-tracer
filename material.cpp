#include "precomp.h"

void Material::setRefractionIndex(float refractionIndex)
{
	float r = (1.0f - refractionIndex) / (1.0f + refractionIndex);
	R0 = r * r;

	r = (refractionIndex - 1.0f) / (refractionIndex + 1.0f);
	R1 = r * r;

	n1divn2 = 1.0f / refractionIndex;
	n2divn1 = refractionIndex;
}
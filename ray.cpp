#include "precomp.h"

void Ray::Cast(Camera* cam, float u, float v)
{
	t = FLT_MAX;
	isInsideObject = false;
	vec3 screenPoint = cam->p0
		+ u * (cam->p1 - cam->p0)
		+ v * (cam->p2 - cam->p0);

	dir = screenPoint - *origin;

	dir.normalize();
}

void Ray::Reset(vec3 d)
{
	t = FLT_MAX;
	dir = d;
}

void Ray::Reflect(vec3 normal)
{
	t = FLT_MAX;
	dir = dir - 2 * dot(dir, normal) * normal;
}

/*
* https://math.stackexchange.com/questions/1163260/random-directions-on-hemisphere-oriented-by-an-arbitrary-vector
*/
void Ray::DiffuseReflect(vec3 normal)
{
	t = FLT_MAX;
	float d;

	vec3 right = cross(normal, vec3(normal.x, normal.z, normal.y)).normalized();
	vec3 up = cross(normal, right).normalized();

	do {
		dir = rng.RandomFloat(FLT_MIN, 1.0f - FLT_MIN) * normal;
		dir += rng.RandomFloat(-1.0f + FLT_MIN, 1.0f - FLT_MIN) * right;
		dir += rng.RandomFloat(-1.0f + FLT_MIN, 1.0f - FLT_MIN) * up;
		d = dir.length();
	} while (d > 1);

	// Normalize it
	dir.x = dir.x / d;
	dir.y = dir.y / d;
	dir.z = dir.z / d;
}

void Ray::CosineWeightedDiffuseReflection(vec3 normal)
{
	float r0 = rng.RandomFloat(0.01, 1), r1 = rng.RandomFloat(0.01, 1);
	float r = sqrt(r0);
	float theta = 2 * PI * r1;
	float x = r * cosf(theta);
	float y = r * sin(theta);
	vec3 localVector = TransformToTangent(normal, vec3(x, y, sqrt(1 - r0)));
	dir.x = localVector.x;
	dir.y = localVector.y;
	dir.z = localVector.z;
}

vec3 Ray::TransformToTangent(vec3 normal, vec3 worldVector)
{
	vec3 N = normal;
	vec3 W;
	if (abs(normal.x > 0.99))
		W = (0, 1, 0);
	else
		W = (1, 0, 0);
	vec3 T = normalize(cross(N, W));
	vec3 B = cross(T, N);
	vec3 localVector = (worldVector * T, worldVector * B, worldVector * N);
	return localVector;
}

/*
 * http://www.flipcode.com/archives/reflection_transmission.pdf
 */
float Ray::Refract(vec3 &normal, float n1divn2, float n2divn1)
{
	t = FLT_MAX;

	float n;
	vec3 norm;
	if (isInsideObject) {
		n = n2divn1;
		norm = -normal;
	}
	else {
		n = n1divn2;
		norm = normal;
	}

	isInsideObject = !isInsideObject;

	float cosI = dot(norm, dir);
	float sinT2 = n * n * (1.0f - cosI * cosI);
	if (sinT2 > 1.0f)
	{
		return -1;
	}
	dir = n * dir - (n * cosI + sqrt(1.0f - sinT2)) * norm;

	return cosI;
}
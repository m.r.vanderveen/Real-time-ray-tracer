#pragma once

struct BVHNode
{
	vec3 bounds_min, bounds_max;
	int firstLeft, count;
	bool Intersect(Ray* r);
	void Traverse(Ray* r, std::vector<Triangle*> &prims, BVHNode* pool, int* indices, vec3 &poi, Material *&m, vec3 &n);
	void Subdivide(std::vector<Triangle*> &prims, uint &poolPtr, BVHNode* pool, int* indices);
	void Partition(std::vector<Triangle*> &prims, int* indices, int &first1, int &count1, int &first2, int &count);
	void CalculateBounds(std::vector<Triangle*> &prims, int* indices);
};
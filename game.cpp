#include "precomp.h" // include (only) this in every .cpp file

static Game* game; // For global reference
std::vector< vec3 > temp_vertices;

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------
void Game::Init()
{
	// Create materials
	Material* blueDiffuse = new Material();
	blueDiffuse->color = vec3(0.05f, 0.05f, 1.0f);

	Material* greenDiffuse = new Material();
	greenDiffuse->color = vec3(0.05f, 1.0f, 0.05f);

	Material* pinkDiffuse = new Material();
	pinkDiffuse->color = vec3(1.0f, 0.05f, 0.5f);

	Material* glass = new Material();
	glass->surface = Material::GLASS;
	glass->setRefractionIndex(1.02f);
	glass->color = vec3(0.6f, 1.0f, 0.6f);

	Material* mirror = new Material();
	mirror->surface = Material::MIRROR;
	mirror->color = vec3(0.95f, 1.0f, 0.95f);

	Material* gold = new Material();
	gold->surface = Material::DIFFUSE;
	gold->specularity = 0.35f;
	gold->color = vec3(1.0f, 0.79f, 0.05f);

	Material* light = new Material();
	light->surface = Material::LIGHT;
	light->emittance = vec3(20.0f, 20.0f, 20.0f);

	Material* light2 = new Material();
	light2->surface = Material::LIGHT;
	light2->emittance = vec3(50.0f, 50.0f, 50.0f);

	// Create primitives
	//OBJLoader("assets/teapot.obj", greenDiffuse, vec3(0, 0, 0));

	/*
	scene.push_back(new Sphere(glass, vec3(-6.0f, 3.0f, 38.0f), 25.0f));
	scene.push_back(new Sphere(pinkDiffuse, vec3(300.0f, 26.0f, 50.0f), 120.0f));
	scene.push_back(new Sphere(greenDiffuse, vec3(-20.0f, 4.0f, 55.0f), 45.0f));	
	scene.push_back(new Plane(new Material(), vec3(0.0f, -180.0f, 0.0f), vec3(0.0f, -1.0f, 0.0f)));
	scene.push_back(new Plane(blueDiffuse, vec3(0.0f, 0.0f, 85.0f), vec3(0.0f, 0.0f, -1.0f)));*/

	float csize = 5;
	float dsize = csize * 2;
	float height = 14.0f - csize * 2;
	/*triangles.push_back(new Triangle(pinkDiffuse, vec3(-csize, 14.0f, 65.0f), vec3(csize, 14.0f, 65.0f), vec3(-csize, height, 65.0f)));
	triangles.push_back(new Triangle(pinkDiffuse, vec3(csize, 14.0f, 65.0f), vec3(csize, height, 65.0f), vec3(-csize, height, 65.0f)));
	
	triangles.push_back(new Triangle(pinkDiffuse, vec3(-csize - 12, height, 65.0f), vec3(-csize - 12, 14.0f, 65.0f - dsize), vec3(-csize - 12, 14.0f, 65.0f)));
	triangles.push_back(new Triangle(pinkDiffuse, vec3(-csize - 12, height, 65.0f - dsize), vec3(-csize - 12, 14.0f, 65.0f - dsize), vec3(-csize - 12, height, 65.0f)));
	
	triangles.push_back(new Triangle(pinkDiffuse, vec3(-csize, 14.0f, 65.0f - dsize), vec3(csize, 14.0f, 65.0f - dsize), vec3(-csize, height, 65.0f - dsize)));
	triangles.push_back(new Triangle(pinkDiffuse, vec3(csize, 14.0f, 65.0f - dsize), vec3(csize, height, 65.0f - dsize), vec3(-csize, height, 65.0f - dsize)));
	
	triangles.push_back(new Triangle(pinkDiffuse, vec3(csize + 2, 14.0f, 65.0f - dsize), vec3(csize + 2, 14.0f, 65.0f), vec3(csize + 2, height, 65.0f)));
	triangles.push_back(new Triangle(pinkDiffuse, vec3(csize + 2, height, 65.0f - dsize), vec3(csize + 2, 14.0f, 65.0f - dsize), vec3(csize + 2, height, 65.0f)));
	
	triangles.push_back(new Triangle(pinkDiffuse, vec3(-csize, height, 65.0f), vec3(csize, height, 65.0f), vec3(csize, height, 65.0f - dsize)));
	triangles.push_back(new Triangle(pinkDiffuse, vec3(-csize, height, 65.0f), vec3(csize, height, 65.0f - dsize), vec3(-csize, height, 65.0f - dsize)));
	
	triangles.push_back(new Triangle(pinkDiffuse, vec3(35.0f, 54.0f, 82.0f), vec3(35.0f, 14.0f, 82.0f), vec3(0.0f, 14.0f, 82.0f)));
	triangles.push_back(new Triangle(pinkDiffuse, vec3(0.0f, 54.0f, 82.0f), vec3(35.0f, 54.0f, 82.0f), vec3(0.0f, 14.0f, 82.0f)));
	*/

	scene.push_back(new Sphere(pinkDiffuse, vec3(300.0f, 120.0f, 50.0f), 15000.0f));
	//scene.push_back(new Sphere(gold, vec3(-6.0f, 120.0f, 38.0f), 10000.0f));
	//scene.push_back(new Sphere(glass, vec3(-206.0f, 120.0f, 138.0f), 10000.0f));

	//scene.push_back(new Sphere(light, vec3(300.0f, 400.0f, 50.0f), 2500.0f));

	//scene.push_back(new Sphere(light, vec3(-200.0f, 400.0f, 38.0f), 2500.0f));

	triangles.push_back(new Triangle(light2, vec3(-100.0f, 1000.0f, 50.0f), vec3(-100.0f, 1000.0f, 300.0f), vec3(150.0f, 1000.0f, 300.0f)));
	triangles.push_back(new Triangle(light2, vec3(-100.0f, 1000.0f, 50.0f), vec3(150.0f, 1000.0f, 50.0f), vec3(150.0f, 1000.0f, 300.0f)));
	//triangles.push_back(new Triangle(light, vec3(400.0f, 500.0f, 80.0f), vec3(580.0f, 350.0f, 190.0f), vec3(250.0f, 500.0f, 220.0f)));
	//triangles.push_back(new Triangle(light, vec3(-850.0f, 300.0f, 40.0f), vec3(-850.0f, -550.0f, 40.0f), vec3(-850.0f, -550.0f, 520.0f)));

	//triangles.push_back(new Triangle(light, vec3(-880.0f, 800.0f, 40.0f), vec3(-880.0f, 50.0f, 40.0f), vec3(-880.0f, 50.0f, 520.0f)));


	//triangles.push_back(new Triangle(mirror, vec3(1000.0f, -37.0f, 300.0f), vec3(-650.0f, -37.0f, 300.0f), vec3(-650.0f, 600.0f, 300.0f)));
	//triangles.push_back(new Triangle(blueDiffuse, vec3(1000.0f, 600.0f, 300.0f), vec3(1000.0f, -37.0f, 300.0f), vec3(-650.0f, 600.0f, 300.0f)));

	//triangles.push_back(new Triangle(blueDiffuse, vec3(1000.0f, -37.0f, 300.0f), vec3(-650.0f, -37.0f, 300.0f), vec3(-650.0f, 600.0f, 300.0f)));
	//triangles.push_back(new Triangle(blueDiffuse, vec3(1000.0f, 600.0f, 300.0f), vec3(1000.0f, -37.0f, 300.0f), vec3(-650.0f, 600.0f, 300.0f)));


	scene.push_back(new Plane(new Material(), vec3(0.0f, -37.0f, 0.0f), vec3(0,1,0)));
	//triangles.push_back(new Triangle(new Material(), vec3(1000.0f, -37.0f, -300.0f), vec3(-650.0f, -37.0f, -300.0f), vec3(-650.0f, -37.0f, 300.0f)));
	//triangles.push_back(new Triangle(new Material(), vec3(1000.0f, -37.0f, 300.0f), vec3(1000.0f, -37.0f, -300.0f), vec3(-650.0f, -37.0f, 300.0f)));

	printf("Objects loaded.\n");

	// Construct BVH
	bvh = new BVH();
	bvh->ConstructBVH(triangles);

	printf("BVH constructed.\n");
	
	//Camera* camera = new Camera(vec3(609, 507, -1085), vec3(-0.249214f, -0.245717f, 0.936758f), 1.5f);
	Camera* camera = new Camera(vec3(-594.331116f, 339.193848f, -387.511169f), vec3(0.830080f, -0.265693f, 0.490280f), 0.9f);
	renderer = new PathTracer(camera, scene, triangles, bvh);

	game = this;
}

// -----------------------------------------------------------
// Close down application
// -----------------------------------------------------------
void Game::Shutdown()
{
}

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------
void Game::Tick( float deltaTime )
{
	// clear the graphics window
	screen->Clear(0);

	// handle camera control
	bool inputRot = false, inputTrans = false, inputFOV = false;	
	inputRot = renderer->camera->Rotate(0.05f);
	inputTrans = renderer->camera->Translate(10.0f);
	inputFOV = renderer->camera->ChangeFOV();
	
	if (inputRot || inputTrans || inputFOV) {
		renderer->camera->Update();
		renderer->Clear();
	}

	// Print camera settings
	if (GetAsyncKeyState(0x50)) {
		printf("---------------------------\n");
		printf("Pos X: %f\n", renderer->camera->pos.x);
		printf("    Y: %f\n", renderer->camera->pos.y);
		printf("    Z: %f\n", renderer->camera->pos.z);
		printf("Dir X: %f\n", renderer->camera->dir.x);
		printf("    Y: %f\n", renderer->camera->dir.y);
		printf("    Z: %f\n", renderer->camera->dir.z);
		printf("FOV: %f\n", renderer->camera->dist);
		printf("---------------------------\n\n");
	}

	// render the scene
	renderer->Render(screen);
}

/*
* This method loads meshes that are saved in an OBJ-file
* It loads a file and reads through it line by line
* Currently it only checks the file for vertices and assembles those into triangles,
* because we don't use textures or pre-defined normals in our triangles class
* The code was made with the tutorial from www.open-gl-tutorial.org
*/
void Game::OBJLoader(const char * path, Material* mat, vec3 position) {
	FILE *file = fopen(path, "r");
	if (file == NULL) {
		printf("No file found");
		perror("Error");
	}

	while (true) {
		char line[128];

		int res = fscanf(file, "%s", line);
		if (res == EOF)
			break;

		if (strcmp(line, "v") == 0) {
			vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}

		if (strcmp(line, "f") == 0) {
			vec3 v1, v2, v3;
			//uint a, b, c, d, e, f, g, h, i;
			//int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &a, &b, &c, &d, &e, &f, &g, &h, &i);
			//triangles.push_back(new Triangle(mat, temp_vertices[a - 1] + position, temp_vertices[d - 1] + position, temp_vertices[g - 1] + position));

			uint a, b, c;
			int matches = fscanf(file, "%d %d %d\n", &b, &a, &c);
			triangles.push_back(new Triangle(mat, temp_vertices[a - 1] + position, temp_vertices[b - 1] + position, temp_vertices[c - 1] + position));
		}
	}
}
#pragma once

// Abstract class
class Geometry
{
public:
	virtual float Intersect(Ray* r, vec3 &poi, Material *&m, vec3 &n) = 0;
	virtual float Intersect(Ray* r) = 0;
	virtual vec3 RandomPointOnSurface(Random &rng, vec3 dir, vec3 &n) = 0;
	virtual bool isAbovePoint(vec3 point, vec3 normal) = 0;
	Material* mat;
	vec3 pos;
	float area;
};

class Sphere : public Geometry
{
public:
	Sphere(Material* material, vec3 position, float radius2);
	float Intersect(Ray* r, vec3 &poi, Material *&m, vec3 &n);
	float Intersect(Ray* r);
	float QuickIntersect(Ray* r);
	vec3 RandomPointOnSurface(Random &rng, vec3 dir, vec3 &n);
	bool isAbovePoint(vec3 point, vec3 normal);
	vec3 orientation;
private:
	float r2;
};

class Plane : public Geometry
{
public:
	Plane(Material* material, vec3 position, vec3 norm);
	float Intersect(Ray* r, vec3 &poi, Material *&m, vec3 &n);
	float Intersect(Ray* r);
	vec3 RandomPointOnSurface(Random &rng, vec3 dir, vec3 &n);
	bool isAbovePoint(vec3 point, vec3 normal);
private:
	vec3 normal;
};

class Triangle : public Geometry
{
public:
	Triangle(Material* material, vec3 position, vec3 position1, vec3 position2);
	float Intersect(Ray* r, vec3 &poi, Material *&m, vec3 &n);
	float Intersect(Ray* r);
	vec3 RandomPointOnSurface(Random &rng, vec3 dir, vec3 &n);
	bool isAbovePoint(vec3 point, vec3 normal);
	vec3 pos1, pos2, edge1, edge2;
private:
	vec3 normal;
};
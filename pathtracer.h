#pragma once

class PathTracer
{
public:
	PathTracer(Camera* cam, std::vector<Geometry*> &scene, std::vector<Triangle*> &triangles, BVH* bvh);
	void Render(Surface* screen);
	void Clear();
	uint PartitionRays(Ray** rayCollection, uint* &rayIndex, BVH* bvh);

	// Path tracing
	bool Trace(Ray* r, vec3 &intersectPoint, Material *&material, vec3 &normal);
	bool TraceLightray(Ray* r, float maxDist);
	vec3 Sample(Ray* r, bool lastSpecular, bool IS, bool NEE, bool RR);
	vec3 RandomPointOnLight(vec3 normal, vec3 poi, vec3 &lightColor, vec3 &Nl, float &dist, float &A);
	Camera* camera;
private:
	bool useNEE = false;
	bool useIS = false;
	bool useRR = false;
	timer tm;
	std::vector<Geometry*> scene;
	std::vector<Triangle*> triangles;
	BVH* bvh;
	std::vector<Geometry*> lights;
	float* lotteryTickets;
	Random rng;
	vec3* screenBuffer;
	float currentSamples;
	float totalEnergy;
	Ray* ray, *reflectRay, *lightRay;
	Ray* rayCollection[SCRHEIGHT * SCRWIDTH];
	float* ucoords;
	float* vcoords;
	bool partition = false;
	uint* rayIndex;
};
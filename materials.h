#pragma once

class Material
{
public:
	enum { DIFFUSE = 0, MIRROR = 1, GLOSSY = 2, GLASS = 3, LIGHT = 4 };
	Material() { color = vec3(0.95f, 0.95f, 0.95f); surface = DIFFUSE; };
	vec3 color;
	bool isLight;
	vec3 emittance;
	int surface;
	float specularity;
	float R0, R1;
	float n1divn2, n2divn1;
	void setRefractionIndex(float refractionIndex);
};
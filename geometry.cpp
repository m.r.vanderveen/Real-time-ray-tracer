#include "precomp.h"

Sphere::Sphere(Material* material, vec3 position, float radius2)
{
	mat = material;
	pos = position;
	r2 = radius2;
	orientation = vec3(0,-1,0);

	// Calculate the area (half)
	area = 2 * PI * r2;
}

/*
 *	http://www.cs.uu.nl/docs/vakken/magr/2017-2018/slides/lecture%2001%20-%20intro%20&%20whitted.pdf
 */
float Sphere::Intersect(Ray* r, vec3 &poi, Material *&m, vec3 &n)
{
	float t = Intersect(r);
	if (t != -1) {
		m = mat;

		// Calculate point of intersection
		poi = *r->origin + t * r->dir;

		// Return normal
		n = (poi - pos).normalized();

	}

	return t;
}

float Sphere::Intersect(Ray* r)
{
	float a = dot(r->dir, r->dir);
	float b = dot(2 * r->dir, *r->origin - pos);
	float c = dot(*r->origin - pos, *r->origin - pos) - r2;

	float part = b*b - 4 * a * c;
	if (part < 0)
		return -1;

	float t1 = (-b + sqrtf(part)) / (2 * a);
	float t2 = (-b - sqrtf(part)) / (2 * a);

	float t = -1;
	if (t1 >= 0 && t2 >= 0)
	{
		t = min(t1, t2);
	}
	else if (t2 < 0 && t1 >= 0)
	{
		t = t1;
	}
	else if (t2 >= 0 && t1 < 0)
	{
		t = t2;
	}

	if ((t < r->t) && (t > EPSILON_INTER))
	{
		r->t = t;
		return t;
	}

	return -1;
}

/*
 *	Bikker's sphere intersection algorithm? (http://www.cs.uu.nl/docs/vakken/magr/2017-2018/slides/lecture%2001%20-%20intro%20&%20whitted.pdf)
 */
float Sphere::QuickIntersect(Ray* r)
{
	vec3 c = pos - *(r->origin);
	float t = dot(c, r->dir);
	vec3 q = c - t * r->dir;
	float p2 = dot(q, q);
	if (p2 > r2) return -1;

	t -= sqrt(r2 - p2);
	if ((t < r->t) && (t > EPSILON_INTER))
	{
		r->t = t;

		return t;
	}

	return -1;
}

vec3 Sphere::RandomPointOnSurface(Random &rng, vec3 dir, vec3 &n)
{
	vec3 p = rng.RandomPointOnHemisphere(dir);

	// Return the normal
	n = p;

	return p * sqrt(r2) + pos;
}

bool Sphere::isAbovePoint(vec3 point, vec3 normal)
{
	return dot(normal, (pos + normal * sqrt(r2)) - point) > 0;
}

Plane::Plane(Material* material, vec3 position, vec3 norm)
{
	mat = material;
	pos = position;
	normal = norm.normalized();
}

/*
 * https://stackoverflow.com/questions/23975555/how-to-do-ray-plane-intersection
 */
float Plane::Intersect(Ray* r, vec3 &poi, Material *&m, vec3 &n)
{
	float t = Intersect(r);
	if (t != -1)
	{
		m = mat;

		// Calculate point of intersection
		poi = *r->origin + t * r->dir;

		// Return normal
		n = normal;
	}
	return t;
}

float Plane::Intersect(Ray *r)
{
	float denom = dot(normal, r->dir);
	if (abs(denom) > EPSILON_INTER)
	{
		float t = dot(pos - *r->origin, normal) / denom;
		if (t < r->t && t > EPSILON_INTER) {
			r->t = t;
			return t;
		}
	}

	return -1;
}

vec3 Plane::RandomPointOnSurface(Random &rng, vec3 dir, vec3 &n)
{
	return vec3(0);
}

bool Plane::isAbovePoint(vec3 point, vec3 normal)
{
	return dot(normal, pos - point) > 0;
}

Triangle::Triangle(Material* material, vec3 position, vec3 position1, vec3 position2)
{
	mat = material;
	pos = position;
	pos1 = position1;
	pos2 = position2;

	edge1 = pos1 - pos;
	edge2 = pos2 - pos;

	// Calcualte normal
	normal = (cross(edge1, edge2)).normalized();

	// Calculate area
	area = cross(pos1 - pos, pos2 - pos).length() * 0.5f;

	printf("%f\n", area);
}

/*
 *	M�ller�Trumbore intersection algorithm (https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm)
 */
float Triangle::Intersect(Ray* r, vec3 &poi, Material *&m, vec3 &n)
{
	float t = Intersect(r);
	if (t != -1)
	{
		m = mat;

		// Calculate point of intersection
		poi = *r->origin + t * r->dir;

		float angle = dot(r->dir, normal);

		if (angle > 0.0f)
			n = -normal;
		else
			n = normal;
	}
	return t;
}

float Triangle::Intersect(Ray* r)
{	
	vec3 h, s, q;
	float a, f, u, v;
	h = r->dir.cross(edge2);
	a = edge1.dot(h);

	if (a > -EPSILON_INTER && a < EPSILON_INTER)
		return -1;

	f = 1 / a;
	s = *r->origin - pos;
	u = f * (s.dot(h));
	if (u < 0.0 || u > 1.0)
		return -1;

	q = s.cross(edge1);
	v = f * r->dir.dot(q);
	if (v < 0.0 || u + v > 1.0)
		return -1;

	// At this stage we can compute t to find out where the intersection point is on the line.
	float t = f * edge2.dot(q);
	if (t < r->t && t > EPSILON_INTER) // ray intersection
	{
		r->t = t;

		return t;
	}
	else // This means that there is a line intersection but not a ray intersection.
		return -1;
}

/*
*	https://adamswaab.wordpress.com/2009/12/11/random-point-in-a-triangle-barycentric-coordinates/
*/
vec3 Triangle::RandomPointOnSurface(Random &rng, vec3 dir, vec3 &n)
{
	float a, b;

	a = rng.RandomFloat(0.0f, 1.0f);
	do {
		b = rng.RandomFloat(0.0f, 1.0f);
	} while (a == b);

	if (a + b >= 1)
	{
		a = 1 - a;
		b = 1 - b;
	}

	float angle = dot(dir, normal);

	if (angle > 0.0f)
		n = normal;
	else
		n = -normal;

	return pos + a * (pos1 - pos) + b * (pos2 - pos);
}

bool Triangle::isAbovePoint(vec3 point, vec3 normal)
{
	return dot(normal, pos - point) > 0 || dot(normal, pos1 - point) > 0 || dot(normal, pos2 - point) > 0;
}
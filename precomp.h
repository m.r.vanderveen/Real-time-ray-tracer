// add your includes to this file instead of to individual .cpp files
// to enjoy the benefits of precompiled headers:
// - fast compilation
// - solve issues with the order of header files once (here)
// do not include headers in header files (ever).

#define SCRWIDTH		800
#define SCRHEIGHT		512
#define ASPECT_RATIO	((float)SCRWIDTH / (float)SCRHEIGHT)
#define VK_A			0x41
#define VK_W			0x57
#define VK_S			0x53
#define VK_D			0x44
#define VK_Q			0x51
#define VK_E			0x45
#define VK_R			0x52
#define VK_F			0x46
#define EPSILON_INTER	0.0000001f
#define EPSILON_SHAD	0.0005f
#define EPSILON			0.05f
#define RECURSION_LIMIT	14

// #define FULLSCREEN
// #define ADVANCEDGL	// faster if your system supports it

#include "omp.h"
#include <vector>
#include <inttypes.h>
extern "C" 
{ 
#include "glew.h" 
}
#include "gl.h"
#include "io.h"
#include <fstream>
#include <iostream>
#include <stdio.h>
#include "fcntl.h"
#include "SDL.h"
#include "wglext.h"
#include "freeimage.h"
#include "math.h"
#include "stdlib.h"
#include "emmintrin.h"
#include "immintrin.h"
#include "windows.h"
#include "template.h"
#include "surface.h"
#include "threads.h"
#include <time.h>
#include <random>
#include <assert.h>

using namespace std;
using namespace Tmpl8;

#include "random.h"
#include "materials.h"
#include "light.h"
#include "camera.h"
#include "ray.h"
#include "geometry.h"
#include "BVHNode.h" 
#include "BVH.h"
#include "pathtracer.h"
#include "raytracer.h"
#include "game.h"
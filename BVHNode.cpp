#include "precomp.h"

void BVHNode::Traverse(Ray* r, std::vector<Triangle*> &prims, BVHNode* pool, int* indices, vec3 &poi, Material *&m, vec3 &n) {
	if (!Intersect(r)) return;
	if (count < 3)
	{
		// This is a leaf node, so intersect primitives
		for (int i = 0; i < count; i++)
			prims[indices[firstLeft + i]]->Intersect(r, poi, m, n);
	}
	else
	{
		pool[firstLeft].Traverse(r, prims, pool, indices, poi, m, n);
		pool[firstLeft + 1].Traverse(r, prims, pool, indices, poi, m, n);
	}
}

/*
*	Bikker's AABB intersection algorithm (http://www.cs.uu.nl/docs/vakken/magr/2017-2018/slides/lecture%2005%20-%20SIMD%20recap.pdf)
*/
bool BVHNode::Intersect(Ray* r)
{
	float tx1 = (bounds_min.x - r->origin->x) / r->dir.x;
	float tx2 = (bounds_max.x - r->origin->x) / r->dir.x;
	float tmin = min(tx1, tx2);
	float tmax = max(tx1, tx2);
	float ty1 = (bounds_min.y - r->origin->y) / r->dir.y;
	float ty2 = (bounds_max.y - r->origin->y) / r->dir.y;
	tmin = max(tmin, min(ty1, ty2));
	tmax = min(tmax, max(ty1, ty2));
	float tz1 = (bounds_min.z - r->origin->z) / r->dir.z;
	float tz2 = (bounds_max.z - r->origin->z) / r->dir.z;
	tmin = max(tmin, min(tz1, tz2));
	tmax = min(tmax, max(tz1, tz2));
	return tmax >= tmin && tmax >= 0;
}

void BVHNode::CalculateBounds(std::vector<Triangle*> &prims, int* indices)
{
	bounds_min = vec3(FLT_MAX);
	bounds_max = vec3(-FLT_MAX);

	for (int k = firstLeft; k < firstLeft + count; k++)
	{
		// Minimum x-coordinate
		if (prims[indices[k]]->pos.x < bounds_min.x)
			bounds_min.x = prims[indices[k]]->pos.x;

		if (prims[indices[k]]->pos1.x < bounds_min.x)
			bounds_min.x = prims[indices[k]]->pos1.x;

		if (prims[indices[k]]->pos2.x < bounds_min.x)
			bounds_min.x = prims[indices[k]]->pos2.x;

		// Minimum y-coordinate
		if (prims[indices[k]]->pos.y < bounds_min.y)
			bounds_min.y = prims[indices[k]]->pos.y;

		if (prims[indices[k]]->pos1.y < bounds_min.y)
			bounds_min.y = prims[indices[k]]->pos1.y;

		if (prims[indices[k]]->pos2.y < bounds_min.y)
			bounds_min.y = prims[indices[k]]->pos2.y;

		// Minimum z-coordinate
		if (prims[indices[k]]->pos.z < bounds_min.z)
			bounds_min.z = prims[indices[k]]->pos.z;

		if (prims[indices[k]]->pos1.z < bounds_min.z)
			bounds_min.z = prims[indices[k]]->pos1.z;

		if (prims[indices[k]]->pos2.z < bounds_min.z)
			bounds_min.z = prims[indices[k]]->pos2.z;


		// Maximum x-coordinate
		if (prims[indices[k]]->pos.x > bounds_max.x)
			bounds_max.x = prims[indices[k]]->pos.x;

		if (prims[indices[k]]->pos1.x > bounds_max.x)
			bounds_max.x = prims[indices[k]]->pos1.x;

		if (prims[indices[k]]->pos2.x > bounds_max.x)
			bounds_max.x = prims[indices[k]]->pos2.x;

		// Maximum y-coordinate
		if (prims[indices[k]]->pos.y > bounds_max.y)
			bounds_max.y = prims[indices[k]]->pos.y;

		if (prims[indices[k]]->pos1.y > bounds_max.y)
			bounds_max.y = prims[indices[k]]->pos1.y;

		if (prims[indices[k]]->pos2.y > bounds_max.y)
			bounds_max.y = prims[indices[k]]->pos2.y;

		// Maximum z-coordinate
		if (prims[indices[k]]->pos.z > bounds_max.z)
			bounds_max.z = prims[indices[k]]->pos.z;

		if (prims[indices[k]]->pos1.z > bounds_max.z)
			bounds_max.z = prims[indices[k]]->pos1.z;

		if (prims[indices[k]]->pos2.z > bounds_max.z)
			bounds_max.z = prims[indices[k]]->pos2.z;
	}
}

void BVHNode::Subdivide(std::vector<Triangle*> &prims, uint &poolPtr, BVHNode* pool, int* indices)
{
	if (count < 3) return;
	uint left = poolPtr++;
	uint right = poolPtr++;

	Partition(prims, indices, pool[left].firstLeft, pool[left].count, pool[right].firstLeft, pool[right].count);

	firstLeft = left;

	if (pool[left].count > 0) pool[left].CalculateBounds(prims, indices);
	if (pool[right].count > 0) pool[right].CalculateBounds(prims, indices);

	pool[left].Subdivide(prims, poolPtr, pool, indices);
	pool[right].Subdivide(prims, poolPtr, pool, indices);
}

void BVHNode::Partition(std::vector<Triangle*> &prims, int* indices, int &first1, int &count1, int &first2, int &count2)
{
	first1 = firstLeft;
	count1 = 0;
	first2 = firstLeft;
	count2 = count;

	vec3 split_pos = vec3(0);
	vec3* tri_centers = new vec3[count];
	Triangle* tri;

	// Calculate median
	for (int i = 0; i < count; i++)
	{
		tri = prims[indices[first1 + i]];
		tri_centers[i] = (tri->pos + tri->pos1 + tri->pos2) / 3;

		split_pos += tri_centers[i];
	}

	split_pos = split_pos / (float)count;

	// Split on largest axis
	float sizeX = bounds_max.x - bounds_min.x;
	float sizeY = bounds_max.y - bounds_min.y;
	float sizeZ = bounds_max.z - bounds_min.z;

	if (sizeX > sizeY && sizeX > sizeZ)
	{
		// split on x
		for (int i = 0; i < count; i++)
		{
			if (tri_centers[i].x <= split_pos.x)
			{
				int temp = indices[first1 + count1];
				indices[first1 + count1] = indices[first1 + i];
				indices[first1 + i] = temp;

				count1++;
				first2++;
				count2--;
			}
		}
	}
	else if (sizeY > sizeX && sizeY > sizeZ)
	{
		// split on y
		for (int i = 0; i < count; i++)
		{
			if (tri_centers[i].y <= split_pos.y)
			{
				int temp = indices[first1 + count1];
				indices[first1 + count1] = indices[first1 + i];
				indices[first1 + i] = temp;

				count1++;
				first2++;
				count2--;
			}
		}
	}
	else
	{
		// split on z
		for (int i = 0; i < count; i++)
		{
			if (tri_centers[i].z <= split_pos.z)
			{
				int temp = indices[first1 + count1];
				indices[first1 + count1] = indices[first1 + i];
				indices[first1 + i] = temp;

				count1++;
				first2++;
				count2--;
			}
		}
	}
}
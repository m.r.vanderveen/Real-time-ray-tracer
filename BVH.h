#pragma once

class BVH
{
public:
	int* indices;
	uint poolPtr;
	BVHNode* pool;
	BVHNode* root;
	void ConstructBVH(std::vector<Triangle*> &prims);
	void BVH::Traverse(Ray* r, std::vector<Triangle*> &prims, vec3 &poi, Material *&m, vec3 &n);
};

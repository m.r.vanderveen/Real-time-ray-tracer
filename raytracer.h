class RayTracer
{
public:
	RayTracer(Camera* cam, std::vector<Geometry*> &scn, std::vector<Triangle*> &tris, BVH* b);
	void Render(Surface* screen);
	vec3 Trace(Ray* r);
	vec3 DirectIllumination(vec3 pos, vec3 norm);
	uint PartitionRays(Ray** rayCollection, uint* &rayIndex, BVH* bvh);
	std::vector<Light*> lights;
	Camera* camera;
private:
	std::vector<Geometry*> scene;
	std::vector<Triangle*> triangles;
	BVH* bvh;
	int recursionCntr;
	Ray* ray, *shadowRay, *reflectRay;
	Ray* rayCollection[SCRHEIGHT * SCRWIDTH];
	float* ucoords;
	float* vcoords;
	bool partition = false;
	uint* rayIndex;
};

#include "precomp.h" 

Camera::Camera(vec3 position, vec3 direction, float distance)
{
	pos = position;
	dir = direction.normalized();
	dist = distance;
	center = pos + dist * dir;
	right = cross(dir, vec3(0, 1, 0)).normalized();
	up = cross(dir, right).normalized();

	p0 = center + dir + up - (-ASPECT_RATIO * right);
	p1 = center + dir + up - ASPECT_RATIO * right;
	p2 = center + dir - up - (-ASPECT_RATIO * right);
}

void Camera::Update()
{
	right = cross(dir, vec3(0, 1, 0)).normalized();
	up = cross(dir, right).normalized();

	center = pos + dist * dir;
	vec3 i = ASPECT_RATIO * right;
	p0 = center + dir + up + i;
	p1 = center + dir + up - i;
	p2 = center + dir - up + i;
}

bool Camera::Rotate(float speed)
{
	bool rotated = false;
	if (GetAsyncKeyState(VK_UP))
	{
		dir -= up * speed / 2;
		rotated = true;
	}

	if (GetAsyncKeyState(VK_DOWN))
	{
		dir += up * speed / 2;
		rotated = true;
	}

	if (GetAsyncKeyState(VK_RIGHT))
	{
		dir += right * speed;
		rotated = true;
	}

	if (GetAsyncKeyState(VK_LEFT))
	{
		dir -= right * speed;
		rotated = true;
	}

	dir.normalize();
	return rotated;
}

bool Camera::ChangeFOV()
{
	bool changed = false;

	if (GetAsyncKeyState(VK_R)) {
		dist += 0.2f;
		changed = true;
	}

	if (GetAsyncKeyState(VK_F) && dist - 0.2f > 0.0f) {
		dist -= 0.2f;
		changed = true;
	}

	return changed;
}

bool Camera::Translate(float speed)
{
	bool translated = false;

	if (GetAsyncKeyState(VK_D)) {
		pos += right * speed;
		translated = true;
	}

	if (GetAsyncKeyState(VK_A)) {
		pos -= right * speed;
		translated = true;
	}

	if (GetAsyncKeyState(VK_Q)) {
		pos.y -= 1 * speed;
		translated = true;
	}

	if (GetAsyncKeyState(VK_E)) {
		pos.y += 1 * speed;
		translated = true;
	}

	if (GetAsyncKeyState(VK_W)) {
		pos += dir * speed;
		translated = true;
	}

	if (GetAsyncKeyState(VK_S)) {
		pos -= dir * speed;
		translated = true;
	}

	return translated;
}

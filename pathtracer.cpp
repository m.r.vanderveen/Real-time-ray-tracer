#include "precomp.h" 

PathTracer::PathTracer(Camera* cam, std::vector<Geometry*> &scn, std::vector<Triangle*> &tris, BVH* b)
{
	camera = cam;
	scene = scn;
	triangles = tris;
	bvh = b;

	// Add lights to lights array
	for (uint k = 0; k < scene.size(); k++)
	{
		if (scene[k]->mat->surface == Material::LIGHT)
			lights.push_back(scene[k]);
	}
	for (uint k = 0; k < triangles.size(); k++)
	{
		if (triangles[k]->mat->surface == Material::LIGHT)
			lights.push_back(triangles[k]);
	}
	lotteryTickets = new float[lights.size()];

	// Allocate screen buffer
	screenBuffer = new vec3[SCRWIDTH * SCRHEIGHT];
	for (int i = 0; i < SCRHEIGHT; i++)
		for (int j = 0; j < SCRWIDTH; j++)
			screenBuffer[i * SCRWIDTH + j] = vec3(0);
	currentSamples = 0;
	totalEnergy = 0;

	// Pre-calculate u,v coordinates
	ucoords = new float[SCRWIDTH];
	vcoords = new float[SCRHEIGHT];
	float u = 0;
	float v = 0;
	float uinc = 1.0f / SCRWIDTH;
	float vinc = 1.0f / SCRHEIGHT;

	for (int i = 0; i < SCRWIDTH; i++) {
		ucoords[i] = u;
		u += uinc;
	}
	for (int i = 0; i < SCRHEIGHT; i++) {
		vcoords[i] = v;
		v += vinc;
	}

	ray = new Ray();
	reflectRay = new Ray();
	lightRay = new Ray();
}

void PathTracer::Render(Surface* screen)
{
	tm.reset();
	totalEnergy = 0;

	if (GetAsyncKeyState(0x4E))
	{
		useNEE = !useNEE;
		Clear();
	}

	// Loop through vertical pixels
	for (int i = 0; i < SCRHEIGHT; i++) {
		// Loop through horizontal pixels
		for (int j = 0; j < SCRWIDTH; j++)
		{
			ray->origin = &(camera->pos);
			ray->Cast(camera, ucoords[j], vcoords[i]);

			// Trace ray
			//if (j > SCRWIDTH / 2)
				screenBuffer[i * SCRWIDTH + j] = (screenBuffer[i * SCRWIDTH + j] * currentSamples + Sample(ray, true, useIS, useNEE, useRR)) / (currentSamples + 1);
			//else
			//	screenBuffer[i * SCRWIDTH + j] = (screenBuffer[i * SCRWIDTH + j] * currentSamples + Sample(ray, true, false)) / (currentSamples + 1);

			totalEnergy += screenBuffer[i * SCRWIDTH + j].x / 3;
			totalEnergy += screenBuffer[i * SCRWIDTH + j].y / 3;
			totalEnergy += screenBuffer[i * SCRWIDTH + j].z / 3;

			// Plot ray color
			screen->Plot(SCRWIDTH - j, SCRHEIGHT - i, ((int)(min(1.0f, screenBuffer[i * SCRWIDTH + j].x) * 255.0f) << 16) + ((int)(min(1.0f, screenBuffer[i * SCRWIDTH + j].y) * 255.0f) << 8) + (int)(min(1.0f, screenBuffer[i * SCRWIDTH + j].z) * 255.0f));						
		}
	}
	
	char tfps[256];
	sprintf(tfps, "%.1f FPS", 1000.0f / tm.elapsed());
	screen->Print(tfps, 10, 10, 0xffffff);

	char te[256];
	sprintf(te, "%.0f energy", totalEnergy);
	screen->Print(te, 10, 20, 0xffffff);

	char tnee[256];
	if (useNEE)
		sprintf(tnee, "NEE - ON");
	else
		sprintf(tnee, "NEE - OFF");
	screen->Print(tnee, 10, 30, 0xffffff);

	currentSamples++;
}

void PathTracer::Clear()
{
	for (int i = 0; i < SCRHEIGHT; i++)
		for (int j = 0; j < SCRWIDTH; j++)
			screenBuffer[i * SCRWIDTH + j] = vec3(0);
	currentSamples = 0;
}

bool PathTracer::Trace(Ray* r, vec3 &intersectPoint, Material *&material, vec3 &normal)
{
	// Loop through non-triangle objects in scene
	for (uint k = 0; k < scene.size(); k++)
	{
		scene[k]->Intersect(r, intersectPoint, material, normal);
	}
	
	// Loop through triangles using a BVH
	bvh->Traverse(r, triangles, intersectPoint, material, normal);	
	
	if (intersectPoint.x == NULL)
		return false;
	else
		return true;
}

bool PathTracer::TraceLightray(Ray* r, float maxDist)
{
	vec3 intersectPoint = NULL;
	Material* material;
	vec3 normal;

	// Loop through non-triangle objects in scene
	for (uint k = 0; k < scene.size(); k++)
	{
		scene[k]->Intersect(r, intersectPoint, material, normal);
	}

	if (intersectPoint.x != NULL && r->t <= maxDist && r->t > EPSILON)
		return true;

	// Loop through triangles using a BVH
	bvh->Traverse(r, triangles, intersectPoint, material, normal);

	if (intersectPoint.x != NULL && r->t <= maxDist && r->t > EPSILON)
		return true;
	else
		return false;
}

vec3 PathTracer::RandomPointOnLight(vec3 normal, vec3 poi, vec3 &lightColor, vec3 &Nl, float &dist, float &A)
{
	vec3 L, point, dir;

	// Calculate area of all lights
	A = 0;
	for (uint i = 0; i < lights.size(); i++)
	{
		A += lights[i]->area;
	}

	// Let lights buy lottery tickets, proportional to area	
	lotteryTickets[0] = lights[0]->area / A;
	for (uint i = 1; i < lights.size() - 1; i++)
	{
		lotteryTickets[i] = lotteryTickets[i - 1] + lights[i]->area / A;
	}
	if (lights.size() > 0)
		lotteryTickets[lights.size() - 1] = 1.0f;

	// Lottery drawing, winner picks a random point on its surface
	float winningTicket = rng.RandomFloat(0.0f, 1.0f);
	uint winner;
	float prev = 0;
	for (uint i = 0; i < lights.size(); i++)
	{
		if (lotteryTickets[i] > prev && winningTicket <= lotteryTickets[i])
		{
			winner = i;
			break;
		}
		prev = lotteryTickets[i];
	}
	//printf("winning tick: %f - %i : %f %f\n", winningTicket, winner, lotteryTickets[0], lotteryTickets[1]);
	dir = (poi - lights[winner]->pos).normalized();

	point = lights[winner]->RandomPointOnSurface(rng, dir, Nl);
	L = point - poi;

	lightColor = lights[winner]->mat->emittance;

	//A = possibleLights[Lid]->area;
	dist = L.length();

	L.x = L.x / dist;
	L.y = L.y / dist;
	L.z = L.z / dist;

	return L;
}

vec3 PathTracer::Sample(Ray* r, bool lastSpecular, bool IS, bool NEE, bool RR)
{	
	vec3 intersectPoint = NULL;
	Material* material;
	vec3 normal;

	// trace ray
	Trace(r, intersectPoint, material, normal);

	if (intersectPoint == NULL) return vec3(0,0,0);

	float survivalRate = 1;
	if(RR)
		float survivalRate = CLAMP(max(material->color.x, material->color.y, material->color.z), 0, 1);
	// terminate if ray left the scene
	//if (rng.RandomFloat(0,1) > survivalRate) return vec3(0,0,0);

	vec3 BRDF = vec3(0), Ei = vec3(0), Ld = vec3(0);
	float PDF;
	switch (material->surface)
	{
		case Material::LIGHT:
			// terminate if we hit a light
			if (NEE)
				return lastSpecular ? material->emittance : vec3(0);
			else
				return material->emittance;

		case Material::DIFFUSE: {
			if (rng.RandomFloat(0, 1) > survivalRate) break;

			float s = material->specularity;

			BRDF = material->color / PI;

			if (rng.RandomFloat(0.0f, 1.0f) <= s) {
				r->Reflect(normal);
				vec3 reflectPoint = intersectPoint + (r->dir * EPSILON);
				r->origin = &reflectPoint;
				// update throughput
				BRDF = material->color;
				Ei = Sample(r, true, IS, NEE, RR);

				return BRDF * Ei;
			}
			else
			{
				// sample a random light source
				if (NEE) {
					vec3 L, Nl, lightColor;
					float dist, A, NdotL;
					L = RandomPointOnLight(normal, intersectPoint, lightColor, Nl, dist, A);

					if (L != NULL)
					{
						lightRay->Reset(L);
						vec3 newOrigin = intersectPoint + (lightRay->dir * EPSILON);
						lightRay->origin = &newOrigin;

						NdotL = dot(normal, L);
						if (NdotL > 0 && dot(Nl, -L) > 0 && !TraceLightray(lightRay, dist - 2 * EPSILON))
						{
							float solidAngle = (dot(Nl, -L) * A) / (dist * dist);
							Ld = lightColor * solidAngle * BRDF * NdotL;
						}
					}
				}

			// continue in random direction
			if (IS)
				r->CosineWeightedDiffuseReflection(normal);
			else
				r->DiffuseReflect(normal);
			vec3 reflectPoint = intersectPoint + (r->dir * EPSILON);

			r->origin = &reflectPoint;

			if (IS)
				PDF = dot(normal, r->dir) / PI;
			else
				PDF = 1 / (2 * PI);

			// update throughput
			Ei = Sample(r, false, IS, NEE, RR) * dot(normal, r->dir);
			break;

			}
		}

		case Material::MIRROR: {
			if (rng.RandomFloat(0, 1) > survivalRate) break;
			r->Reflect(normal);
			vec3 reflectPoint = intersectPoint + (r->dir * EPSILON);
			r->origin = &reflectPoint;
			// update throughput
			BRDF = material->color;
			Ei = Sample(r, true, IS, NEE, RR);

			return (BRDF * Ei)  * (1 / survivalRate);
		}

		case Material::GLASS: {
			float cosI = dot(normal, r->dir);
			if (cosI == -1)
				return vec3(0, 0, 0);

			float R0;
			if (r->isInsideObject)
				R0 = material->R1;
			else
				R0 = material->R0;

			// Reflect or refract based on Fresnel (Schlick's approx.)
			if (rng.RandomFloat(0.0f, 1.0f) <= R0 + (1.0f - R0) * pow(1.0f - abs(cosI), 5))
			{
				r->Reflect(normal);
			}
			else
			{
				r->Refract(normal, material->n1divn2, material->n2divn1);
			}		

			vec3 newOrigin = intersectPoint + (r->dir * EPSILON);
			r->origin = &newOrigin;

			// update throughput
			BRDF = material->color;
			Ei = Sample(r, true, IS, NEE, RR);

			return BRDF * Ei;
		}
	}
	return (BRDF * (Ei / PDF) + (NEE ? Ld : 0)) * (1 / survivalRate);
}
#include "precomp.h"

void BVH::ConstructBVH(std::vector<Triangle*> &prims)
{
	int N = prims.size();

	// create index array
	indices = new int[N];
	for (int i = 0; i < N; i++) indices[i] = i;

	// allocate BVH root node
	pool = (BVHNode*)_mm_malloc(sizeof(BVHNode) * N * 2 - 1, 32);
	//pool = new BVHNode[N * 2 - 1];

	root = &pool[0];
	poolPtr = 2;

	// subdivide root node
	root->firstLeft = 0;
	root->count = N;
	root->CalculateBounds(prims, indices);
	root->Subdivide(prims, poolPtr, pool, indices);
}

void BVH::Traverse(Ray* r, std::vector<Triangle*> &prims, vec3 &poi, Material *&m, vec3 &n)
{
	root->Traverse(r, prims, pool, indices, poi, m, n);
}